import org.junit.Test;
import smth.Factorial;
import static org.junit.Assert.assertEquals;

public class FactorialTest {

    @Test
    public void testFactorial1(){
        assertEquals(15, Factorial.factorial(4,5));
    }
    @Test
    public void testFactorial2(){
        assertEquals(2, Factorial.factorial(1,1));
    }
    @Test
    public void testFactorial3(){
        assertEquals(48, Factorial.factorial(15,3));
    }




}
