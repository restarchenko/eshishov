import org.junit.Assert;
import org.junit.Test;
import smth.Febunachi;

public class FebunachiTest {
    int[] febunNumbers = new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21};

    @Test
    public void testFebunachi1(){
        Assert.assertArrayEquals(febunNumbers, Febunachi.febunachi(-1,3));
    }
    @Test
    public void testFebunachi2(){
        Assert.assertArrayEquals(febunNumbers, Febunachi.febunachi(40,4));
    }
    @Test
    public void testFebunachi3(){
        Assert.assertArrayEquals(febunNumbers, Febunachi.febunachi(-1,6));
    }
}
