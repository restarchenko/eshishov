package smth;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        int algorithmId = Integer.parseInt(args[0]);
        int loopType = Integer.parseInt(args[1]);
        int n = Integer.parseInt(args[2]);
        try {
            if (algorithmId == 1 ){
                System.out.println(Arrays.toString(Febunachi.febunachi(loopType, n)));
            } else if(algorithmId == 2){
                System.out.println(Factorial.factorial(loopType,n));
            } else System.out.println("Something went wrong!");
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

    }
}

