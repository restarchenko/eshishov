package smth;

public class Factorial {
    public static int factorial(int loopType, int n){
        if(loopType < 0) throw new ArithmeticException("The value is less than 0");
        if(n < 0) throw new ArithmeticException("The value is less than 0");

        int i = 1;
        int fact = 1;
        if (loopType == 1){
            while (i < n){
                fact = fact*i;
                i++;
            }
        } else if (loopType == 2){
            do {
                fact = fact*i;
                i++;
            }
            while (i < n);
        } else {
            for ( i = 0; i < n; i++){
                fact = fact*i;
            }
        }
        return fact;
    }
}
