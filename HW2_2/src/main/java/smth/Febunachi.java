package smth;

public class Febunachi {
    public static int[] febunachi(int loopType,int n){
        if(loopType <= 0) throw new IllegalArgumentException("Negative numbers are not allowed");;
        if(n <= 0) throw new IllegalArgumentException("Negative numbers are not allowed");;

        int[] febun = new int[n];
        int previousNumber = 0;
        int nextNumber = 1;
        int i = 0;
        if (loopType == 1){
            while(i < n){
                febun[i] = previousNumber;
                int temp = nextNumber + previousNumber;
                previousNumber = nextNumber;
                nextNumber = temp;
                i++;
            }
        } else if (loopType == 2){
            do{
                febun[i] = previousNumber;
                int temp = nextNumber + previousNumber;
                previousNumber = nextNumber;
                nextNumber = temp;
                i++;
            }
            while(i < n);
        } else {
            for(int k = 0; k < n; k++){
                febun[k] = previousNumber;
                int temp = nextNumber + previousNumber;
                previousNumber = nextNumber;
                nextNumber = temp;
            }
        }
        return febun;
    }
}
